# Installing

To install my dotfiles in the current users home, simply run
```bash
    git clone git@bitbucket.org:rauch/dotfiles.git
    ./dotfiles/install
```
The installer is using [dotbot](https://github.com/anishathalye/dotbot), you
can also run `./dotfiles/install --help` to get a list of more available
options.